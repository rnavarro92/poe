// JavaScript Document
//mejorando.la/node.js/servidor.js
var http = require('http').createServer(handler);
var io = require('socket.io').listen(http);
var fs = require('fs');
var url = require("url");
var path = require('path');

// on server started we can load our node.html page
function handler ( req, res ) {
	var postData = "";
	var pathname = url.parse(req.url).pathname;
	
	console.log('Request for ' + pathname + ' received.');
	req.addListener('data', function(data) {
		postData += data;
		console.log('Received POST data chunk ' + postData + '.');
	});
		
	req.addListener('end', function() {
		var pathext = path.extname(pathname);
		if (pathext === '.js' || pathext === '.css') {
			handleStaticPages(pathname, res);
		} else {
			console.log('--> Routing only the view page');
			fs.readFile( __dirname + '/node.html' , function ( err, data ) {
				if ( err ) {
					console.log( err );
					res.writeHead(500);
					return res.end( 'Error loading node.html' );
				}
				res.writeHead( 200 );
				res.end( data );
			});
		}
	});
}

function handleStaticPages(pathname, res) {
	var ext = path.extname(pathname);
	switch(ext) {
		case '.css':
			res.writeHead(200, {"Content-Type": "text/css"});
			fs.readFile('./' + pathname, 'utf8', function(err, fd) {
				res.end(fd);
			});
			console.log('Routed for Cascading Style Sheet '+ pathname +' Successfully\n');
		break;
		case '.js':
			res.writeHead(200, {"Content-Type": "text/javascript"});
			fs.readFile('./' + pathname, 'utf8', function(err, fd) {
				res.end(fd);
			});
			console.log('Routed for Javascript '+ pathname +' Successfully\n');
		break;
	}
}
	
io.sockets.on("connection", arranque);

function arranque(socket) {
	socket.on("nuevoNombre", emitir);
}
function emitir(data) {
	io.sockets.emit("nombreDesdeServidor", data);
}

http.listen(8000, function() {
  console.log('listening on *:8000');
});