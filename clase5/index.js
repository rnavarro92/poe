var server = require("./server");
var router = require("./router");
var requestHandlers = require("./requestHandlers");
var handler = {};
handler["/insertar"] = requestHandlers.insertar;
handler["/seleccionar"] = requestHandlers.seleccionar;
server.start(router.route, handler);