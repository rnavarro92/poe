var http = require("http");
var url = require("url");
var path = require('path');
var fs = require('fs');

function serverStart(route, handler) {
    //http.createServer(function(request, response) {
    //    var pathname = url.parse(request.url).pathname;
        
    //    route(handler, pathname, request, response);
        
    //}).listen(4444);
    //console.log("Server started...");
    
    function handleStaticPages(pathName, res) {
        var ext = path.extname(pathName);
        switch(ext) {
            case '.css':
                res.writeHead(200, {"Content-Type": "text/css"});
                fs.readFile('./' + pathName, 'utf8', function(err, fd) {
                    res.end(fd);
                });
                console.log('Routed for Cascading Style Sheet '+ pathName +' Successfully\n');
            break;
            case '.js':
                res.writeHead(200, {"Content-Type": "text/javascript"});
                fs.readFile('./' + pathName, 'utf8', function(err, fd) {
                    res.end(fd);
                });
                console.log('Routed for Javascript '+ pathName +' Successfully\n');
            break;
            case '.map':
                res.writeHead(200, {"Content-Type": "text/map"});
                fs.readFile('./' + pathName, 'utf8', function(err, fd) {
                    res.end(fd);
                });
                console.log('Routed for map '+ pathName +' Successfully\n');
            break;
        }
    }
    
    function onRequest(req, res) {
        var postData = "",
            pathName = url.parse(req.url).pathname;
        console.log('Request for ' + pathName + ' received.');

        req.addListener('data', function(data) {
            postData += data;
            console.log('Received POST data chunk ' + postData + '.');
        });

        req.addListener('end', function() {
            var pathext = path.extname(pathName);
            if (pathext === '.js' || pathext === '.css' || pathext === '.map') {
                handleStaticPages(pathName, res);
            } else {
                console.log('--> Routing only the view page');
                route(handler, pathName, req, res);
            }
        });
    }
    http.createServer(onRequest).listen(4444);
    console.log('Server is now listening at: http://127.0.0.1:4444 .');
}
exports.start = serverStart;