var mysql = require('mysql');
var fs = require('fs');
var url = require("url");

//Parametros de conexion a BD mysql.
var connection = mysql.createConnection({
    host		: 'localhost',
    user		: 'root',
	password	: '',
	database	: 'prueba_node'
});

//Si ha ocurrido un error.
connection.connect(function(error){
   if(error){
      throw error;
   }else{
      console.log('Conexion correcta.');
   }
});

function insertar(request, response) {
    console.log("Handler for request 'insertar' dispatched.");
    
    connection.query('INSERT INTO personaje(nombre, apellido, biografia) VALUES(?, ?, ?)',
            ['Homero', 'Simpson', 'Esposo de Marge y padre de Bart, Lisa y Maggie.'],
                function(error, result){
                    if(error){
                       throw error;
                    }else{
                       console.log(result);
                    }
                }
            );
    //response.writeHead(200, {"Content-Type": "text/html"});
    //response.write("Se ha insertado un registro");
    //response.end();
    
    fs.readFile( __dirname + '/insertar.html' , function ( err, data ) {
		if ( err ) {
			console.log( err );
			response.writeHead(500);
			return response.end( 'Error loading insertar.html' );
		}
		response.writeHead(200, {"Content-Type": "text/html"});
		response.end(data);
	});
    
}

function seleccionar(request, response) {
    var query = url.parse(request.url, true).query;
    console.log("Handler for request 'seleccionar' dispatched."+query.id);
    response.writeHead(200, {"Content-Type": "text/html"});
    
    fs.readFile( __dirname + '/insertar.html' , function (err, data) {
        connection.query("SELECT nombre, apellido, biografia FROM personaje WHERE personaje_id="+"'"+query.id+"'",
            function(error, result){
                if(error){
                   throw error;
                }else{
                   var personajes = result;
                   var cadena = "Se ha hecho un select y los resultados son: <br>";
                   if(personajes.length > 0){
                        for(personaje in personajes) {
                            cadena += personajes[personaje[0]].nombre + ' ' + personajes[personaje[0]].apellido + ' / ' + personajes[personaje[0]].biografia + "<br>";
                        }
                        if ( err ) {
                            console.log( err );
                            response.writeHead(500);
                            return response.end( 'Error loading insertar.html' );
                        }
                        response.write(cadena);
                        response.end(data);
                    }else{
                        console.log('Registro no encontrado');
                        response.write("Registro no encontrado");
                        response.end(data);
                     }
                }
            }
        );
    }); 
}
exports.insertar = insertar;
exports.seleccionar = seleccionar;