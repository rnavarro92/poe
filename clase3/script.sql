CREATE TABLE personaje(
    personaje_id smallint(5) unsigned not null auto_increment primary key,
    nombre varchar(50) not null,
    apellido varchar(50) not null,
    biografia varchar(50) not null
);