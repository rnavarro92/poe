var mysql = require('mysql');

var connection = mysql.createConnection({
    host        : 'localhost',
    user        : 'root',
    password    : '',
    database    : 'bd_nodejs'
});

connection.connect(function(error){
    if(error){
        throw error;
    }else{
        console.log('Conexion correcta');
    }
});

function insertar(response){
    
    console.log("Handler for request 'Insertar' dispatched.");
    
    connection.query('INSERT INTO personaje(nombre, apellido, biografia) VALUES(?,?,?)',
                                  ['Homero', 'Simpson', 'Esposo de Marge y padre de Bart, Lisa y Magiie.'],
                                  function(error, result){
                                    if(error){
                                        throw error;
                                    }else{
                                        console.log(result);
                                    }
                                  }
                                );
    response.writeHead(200, {"Content-type": "text/html"});
    response.write("Se ha insertado un registro");
    response.end();
    
}
function seleccionar(response){
    
    console.log("Handler for request 'Seleccionar' dispatched.");
    response.writeHead(200, {"Content-type": "text/html"});
    connection.query('SELECT nombre, apellido, biografia FROM personaje',
                     function(error, result){
                        if(error){
                            throw error;
                        }else{
                            var personajes = result;
                            var cadena = "Se ha hecho un select y los resultados son: <br>";
                            if(personajes.length > 0){
                                for(personaje in personajes){
                                    cadena += personajes[personaje[0]].nombre +
                                    ' ' + personajes[personaje[0]].apellido +
                                    ' ' + personajes[personaje[0]].biografia + "<br>";
                                }
                                response.write(cadena);
                                response.end();
                            }else{
                                console.log("Registro no encontrado");
                                response.write("Registro no encontrado");
                                response.end();
                            }
                        }
                    
                     }
                );
}

exports.insertar = insertar;
exports.seleccionar = seleccionar;