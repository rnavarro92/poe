var port 				= process.env.PORT || 8000,
	express 			= require('express'),
	app 					= express(),				
	mysql					= require('mysql');
	
	connection			= mysql.createConnection({
		host		: 'localhost',
		user		: 'root',
		password	: 'nicolas88',
		database	: 'nodeexpress'
	});
	
app.listen(port, function(){
	console.log('listening on *:8000');
});

// If there is an error connecting to the database
connection.connect(function(err) {
	// connected! (unless `err` is set)
	if(err){
	   throw err;
	}else{
	   console.log('Conexion correcta.');
	}
});

require('./config')(app);
require('./routes')(connection, app);
