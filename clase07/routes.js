// This file is required by app.js. It sets up event listeners


// Export a function, so that we can pass 
// the app and io instances from the app.js file:

module.exports = function(connection, app){

	var userSelect = [];
	
	// on server started we can load our client.html page
	app.get(['/','/select'], function(req, res){
		// Render views/select.html
		//userSelect = [];
		selectAllUsers(res);
	});
	
	/*app.param('id', function (req, res, next, id) {
		userSelect = [];
		selectUserID(id);
		next();
	});*/
	
	app.get('/select/:id', function(req, res){
		// Render views/client.html
		console.log("ID: "+ req.params.id);
		selectUserID(res, req.params.id);
	});
	
	app.use('/insert', function(req,res, next){
		// Render the views/insert.html
		res.render('insert');
		next();
	});
	
	app.post('/insert', function(req, res) {
		console.log("name: " + req.body.username);
		res.end("ok");
	});
	
	var selectUserID = function (res, id) { 
		connection.query("SELECT * FROM users WHERE user_id='"+id+"' ORDER BY user_id DESC",
			function(error, result){
				if(error){
				   throw error;
				}else{
					userSelect = result;
					res.render('select', { users : userSelect });
				}
			}
		);
	};
	
	/*
	*
	* HERE IT IS THE COOL PART
	* This function loops on itself since there are sockets connected to the page
	* sending the result of the database query after a constant interval
	*
	*/
	var selectAllUsers = function (res) {
		// Doing the database query
		var query = connection.query('SELECT * FROM users ORDER BY user_id DESC'),
		users = [];
	
		// setting the query listeners
		query
		.on('error', function(err) {
			// Handle error, and 'end' event will be emitted after this as well
			console.log( err );
		})
		.on('result', function( user ) {
			// it fills our array looping on each user row inside the db
			console.log("Se logroooo!!");
			users.push( user );
		})
		.on('end',function(){
			userSelect = users;
			res.render('select', { users : userSelect });
		});
	};
};
