var server= require("./server");
var router= require("./router");
var requestHandler= require("./requestHandlers");
var handler = {};

handler["/insertar"] = requestHandler.insertar;
handler["/seleccionar"] = requestHandler.seleccionar;
handler["/actualizar"] = requestHandler.actualizar;
handler["/eliminar"] = requestHandler.eliminar;

server.start(router.route, handler);