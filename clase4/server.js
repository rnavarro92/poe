var http = require('http');
var url = require('url');

function serverStart(route, handler){
	
	http.createServer(function(request, response){
		var pathname = url.parse(request.url).pathname;
		var query = url.parse(request.url, true).query;

		route(handler, pathname, response, query);
		
	}).listen(4444);

	console.log('Server started...');

}

exports.start = serverStart;

