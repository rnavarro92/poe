var mysql = require('mysql');


var connection = mysql.createConnection({
	host		: 'localhost',
	user		: 'root',
	password	: '',
	database	: 'bd_nodejs'
});

connection.connect(function(error){
	if(error){
		throw error;
	}else{
		console.log('Conexion correcta.');
	}
});
function insertar(response){
	console.log("Handler for request 'insertar' dispatched.");

	connection.query('INSERT INTO personaje(nombre, apellido, biografia) VALUES(?, ?, ?)',
		['Homero', 'Simpson', 'Esposo de Marge y padre de Bart, Lisa y Maggie.'],
			function(error, result){
				if(error){
					throw error;
				}else{
					console.log(result);
				}
			});
	response.writeHead(200, {"Content-Type": "text/html"});
	response.write("Se ha insertadoun registro");
	response.end();
}

function seleccionar(response){
	console.log("Handler for request 'seleccionar' dispatched");
	
	response.writeHead(200, {"Content-Type": "text/html"});

	connection.query('SELECT nombre, apellido, biografia FROM personaje',
		function(error, result){
			if(error){
				throw error;
			}else{
				var personajes = result;
				var cadena = "Se ha hecho un select y los resultados son: <br>";
				if(personajes.length > 0){
					for(personaje in personajes){
						cadena += personajes[personaje[0]].nombre +
							' ' + personajes[personaje[0]].apellido +
							' ' + personajes[personaje[0]].biografia + '<br>';
					}

					response.write(cadena);
					response.end();
				}else{
					console.log('Registro no encontrado');
					response.write("Registro no encontrado");
					responde.end();
				}
			}
		}
		);
	// response.end();

}

function actualizar(response, query){
	console.log("Handler for request 'actualizar' dispatched");
	
	connection.query('UPDATE personaje SET nombre=?, apellido=?, biografia=? WHERE personaje_id=?',
		['algo', 'algo', 'algo', query.id],
			function(error, result){
				if(error){
					throw error;
				}else{
					console.log("Se actualizo la entrada" + result);
				}
			});

	response.writeHead(200, {"Content-Type": "text/html"});
	response.write("Se ha actualizado un registro");
	response.end();

}

function eliminar(response, query){
	console.log("Handler for request 'eliminar' dispatched");

	response.writeHead(200, {"Content-Type": "text/html"});
	connection.query('DELETE FROM personaje WHERE personaje_id=?',
		[query.id],
			function(error, result){
				if(error){
					throw error;
				}else{
					console.log("Se elimino"+result);
				}
			});
	response.writeHead(200, {"Content-Type": "text/html"});
	response.write("Se ha eliminado un registro de bd");
	response.end();
}

exports.insertar = insertar;
exports.seleccionar = seleccionar;
exports.actualizar = actualizar;
exports.eliminar = eliminar;