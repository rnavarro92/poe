// This file is required by app.js. It sets up event listeners


// Export a function, so that we can pass 
// the app and io instances from the app.js file:

module.exports = function(connection, app){

	var userSelect = [];
	var userUpdate = [];
	var userDelete = [];
	
	app.get(['/home'], function(req, res){
		res.render('home');
	});


	// on server started we can load our client.html page
	
	app.get(['/','/select'], function(req, res){
		// Render views/select.html
		//userSelect = [];
		selectAllUsers(res);
	});
	
	/*app.param('id', function (req, res, next, id) {
		userSelect = [];
		selectUserID(id);
		next();
	});*/
	
	app.get('/update/:id', function(req, res){
		console.log("ID: " + req.params.id);
		updateUserID(res, req.params.id);
	});
	
	app.get('/delete/:id', function(req, res){
		console.log('ID a eliminar: ' + req.params.id);
		deleteUserID(res, req.params.id);
	});
	
	app.get('/select/:id', function(req, res){
		// Render views/client.html
		console.log("ID: "+ req.params.id);
		selectUserID(res, req.params.id);
	});
	
	app.use('/delete/:id', function(req, res, next){
			console.log('Esta es mi id a eliminar' + req.params.id);
			next();
	});
	
	app.post('/delete/:id', function(req, res){
		connection.query('DELETE FROM users WHERE user_id=?', [req.params.id],
						 function(error, result){
							if(error){
								throw error;
							}else{
								console.log(result);
								res.redirect('/');
							}
						 });
		
	});
	
	app.use('/update/:id', function(req, res, next){
		console.log('Estas es mi id: ' + req.params.id);
		next();
	});
	
	app.post('/update/:id', function(req, res){
		console.log("name: " + req.body.nombre);
		console.log("descripcion: " + req.body.desc);
		console.log("link: " + req.body.link);
		console.log("ID algo: " + req.params.id);
		connection.query('UPDATE users SET user_name=?, user_description=?, user_img=? WHERE user_id=?', [req.body.nombre, req.body.desc, req.body.link, req.params.id],
						 function(error, result){
							if(error){
								throw error;
							}else{
								console.log(result);
								res.redirect('/');
							}
						 });
		
		
	});
	
	
	
	app.use('/insert', function(req,res, next){
		// Render the views/insert.html
		res.render('insert');
		next();
	});
	
	app.post('/insert', function(req, res) {
		console.log("name: " + req.body.username);
		console.log("descripcion: " + req.body.desc);
		console.log("link: " + req.body.link);
		connection.query('INSERT INTO users (user_name, user_description, user_img) VALUES (?,?,?)',[req.body.username, req.body.desc, req.body.link],
						 function(error, result){
				if(error){
					throw error;
				}else{
					console.log(result);
				}
			});
		
		res.end("ok");
	});
	
	
	var deleteUserID = function (res, id){
		connection.query("SELECT * FROM users WHERE user_id='"+id+"' ORDER BY user_id DESC",
			function(error, result){
				if(error){
					throw error;
				}else{
					userDelete = result;
					res.render('delete', { users : userDelete });
				}
			}
		);
	};
	
	var updateUserID = function (res, id){
		connection.query("SELECT * FROM users WHERE user_id='"+id+"' ORDER BY user_id DESC",
			function(error, result){
				if(error){
					throw error;
				}else{
					userUpdate = result;
					res.render('update', { users : userUpdate });
				}
			}
		);
	};
	
	var selectUserID = function (res, id) { 
		connection.query("SELECT * FROM users WHERE user_id='"+id+"' ORDER BY user_id DESC",
			function(error, result){
				if(error){
				   throw error;
				}else{
					userSelect = result;
					res.render('select', { users : userSelect });
				}
			}
		);
	};
	
	/*
	*
	* HERE IT IS THE COOL PART
	* This function loops on itself since there are sockets connected to the page
	* sending the result of the database query after a constant interval
	*
	*/
	var selectAllUsers = function (res) {
		// Doing the database query
		var query = connection.query('SELECT * FROM users ORDER BY user_id DESC'),
		users = [];
	
		// setting the query listeners
		query
		.on('error', function(err) {
			// Handle error, and 'end' event will be emitted after this as well
			console.log( err );
		})
		.on('result', function( user ) {
			// it fills our array looping on each user row inside the db
			console.log("Se logroooo!!");
			users.push( user );
		})
		.on('end',function(){
			userSelect = users;
			res.render('select', { users : userSelect });
		});
	};
};
