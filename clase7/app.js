var port 				= process.env.PORT || 8000,
	express 			= require('express'),
	app 					= express(),
	io					= require('socket.io').listen(app.listen(port, function(){
	console.log('listening on *:8000');
})),
	mysql					= require('mysql');
	
	connection			= mysql.createConnection({
		host		: 'localhost',
		user		: 'root',
		password	: '',
		database	: 'node_express'
	});


// If there is an error connecting to the database
connection.connect(function(err) {
	// connected! (unless `err` is set)
	if(err){
	   throw err;
	}else{
	   console.log('Conexion correcta.');
	}
});

require('./config')(app);
require('./routes')(connection, app);
require('./events')(io);
