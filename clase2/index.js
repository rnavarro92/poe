var server = require("./server");
var router = require("./router");
var requestHandlers = require("./requestHandlers");
var handler = {};
handler["/"] = requestHandlers.escribealgo;
handler["/quesucede"] = requestHandlers.quesucede;
handler["/disimula"] = requestHandlers.disimula;
server.start(router.route, handler);