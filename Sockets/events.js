//Escucha los sockets desde el servidor
module.exports=function(io){
	//usamos la variable importada mediante el evento 'connection' pertence a la biblioteca de sockets
	io.sockets.on('connection', function(socket) {
		//verificamos que cargue el evento de una nueva conexion de un cliente
		console.log('Bienvenido');
		//emitimos un evento llamado 'event-example' que lleva consigo el String 'Hola eso es....'
		socket.emit('event-example','Hola eso es un ejemplo con sockets');
		//Escuchamos la desconexion de los clientes, evento 'disconnect' es de la biblioteca de sockets
		socket.on('disconnect', function() {
		  console.log('user disconnected socket');
		});
	});
};