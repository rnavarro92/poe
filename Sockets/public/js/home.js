$(document).ready(function () {
  //Tambien cuando conectamos un socket a nuestro servidor y no estamos en el
  //mismo equipo tenemos que ingresar la ip de nuestro servidor para de esta
  //forma conectar los sockets y en este caso io.connect('192.168.1.252:4444')
  //recibe la ip del servidor + el puerto donde esta corriendo node
  var websocket = io.connect();
  websocket.on('event-example', function (datoDesdeEvento) {
    window.alert(datoDesdeEvento);
  });
});