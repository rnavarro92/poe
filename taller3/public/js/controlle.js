(function (){

var app = angular.module('app', []);

app.factory('socket', function(){ // sirve para utilizar librerias que no son de angular

	var socket = io();

});

app.controller("appChat", function($scope, socket){
	$scope.apellido = "Navarro";
	$scope.username = "Ricardo";
	$scope.mensajes = [];

	$scope.enviarMensaje = function () {
		socket.emit('chat-message', $scope.message);
		console.log("Envie: " + $scope.message);
		$scope.message = "";
	};

	socket.on('chat-message', function(msg){

		console.log("Recibo: " + msg);
		$scope.mensajes.push(msg);
		$scope.$digest();

	});

});

});